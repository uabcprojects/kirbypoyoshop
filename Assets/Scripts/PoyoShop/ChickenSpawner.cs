using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChickenSpawner : MonoBehaviour
{
    public GameObject chickenPrefab;
    public void SpawnChicken(){
        Instantiate(chickenPrefab, transform);
    }
}
