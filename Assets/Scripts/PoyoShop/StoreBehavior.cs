using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoreBehavior : MonoBehaviour
{

    [Header("Random time to prepare poyo")]
    public float minTime = 1f;
    public float maxTime = 5f;
    public IEnumerator OrderPoyos(KirbyShoppingBehavior kirbyShoppingBehavior, int poyosToShop){
        for (int i = 0; i < poyosToShop; i++)
        {
            yield return new WaitForSeconds(Random.Range(minTime, maxTime));
            kirbyShoppingBehavior.ReceivePoyo();
        }
        yield return null;
    }
}
