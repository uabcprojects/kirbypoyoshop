using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KirbyShoppingBehavior : MonoBehaviour
{

    private KirbyEvents events;
    public StoreBehavior storeBehavior = null;
    public BoxCollider2D stopTrigger = null;
    private int poyosToShop = 1;
    private AudioSource audioSource = null;
    private ChickenSpawner chickenSpawner = null;
    [Header("Random poyos to shop")]
    public int minPoyos = 1;
    public int maxPoyos = 3;
    [Header("Kirby Sounds")]
    public AudioClip kirbyPoyoSound;
    public AudioClip kirbyHiSound;

    void Awake(){
        events = GetComponent<KirbyEvents>();

        storeBehavior = FindObjectOfType<StoreBehavior>();
        audioSource = GetComponent<AudioSource>();
        chickenSpawner = GetComponentInChildren<ChickenSpawner>();

        poyosToShop = Random.Range(minPoyos, maxPoyos + 1);

        events.kirbyArrivesToShop += Shop;
    }

    void Shop(){
        audioSource.clip = kirbyHiSound;
        audioSource.Play();
        StartCoroutine(storeBehavior.OrderPoyos(this, poyosToShop));
    }

    public void ReceivePoyo(){
        audioSource.clip = kirbyPoyoSound;
        // Receive poyo animation
        audioSource.Play();
        chickenSpawner.SpawnChicken();
        poyosToShop--;
        if(poyosToShop <= 0){
            StartCoroutine(LeaveStore());
        }
    }

    public IEnumerator LeaveStore(){
        yield return new WaitForSeconds(0.5f);
        stopTrigger.enabled = false;
        events.leaveStore.Invoke();
    }
}
