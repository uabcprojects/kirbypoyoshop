using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KirbyGenerator : MonoBehaviour
{

    public GameObject kirbyPrefab = null;
    
    [Header("Kirby Random Spawn Time")]
    public float minTime = 1f;
    public float maxTime = 5f;
    public bool spawnKirbys = true;
    private bool hasKirbyWaiting = false;

    void Start()
    {
        StartCoroutine(GenerateKirbys());
    }

    private IEnumerator GenerateKirbys(){
        while(true){
            if(spawnKirbys){
                yield return new WaitForSeconds(Random.Range(minTime, maxTime));
                yield return new WaitUntil(() => CanSpawnKirby());
                Instantiate(kirbyPrefab, transform);
            }
        }
    }

    private bool CanSpawnKirby(){
        return !hasKirbyWaiting;
    }

    void OnTriggerStay2D(Collider2D collider){
        hasKirbyWaiting = true;
    }

    void OnTriggerExit2D(Collider2D other){
        hasKirbyWaiting = false;
    }
}
