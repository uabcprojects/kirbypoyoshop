using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KirbyMovement : MonoBehaviour
{
    private KirbyEvents events;
    public float speed = 1f;
    private Rigidbody2D rb;
    private Animator animator;
    void Awake(){
        events = GetComponent<KirbyEvents>();

        if(rb == null){
            rb = GetComponent<Rigidbody2D>();
        }
        if(animator == null){
            animator = GetComponent<Animator>();
        }

        events.leaveStore += Move;
    }

    void Start()
    {
        Move();
    }

    public void Move(){
        rb.velocity = new Vector2(speed, 0);
        animator.SetBool("IsMoving", true);
    }

    public void StopMoving(){
        rb.velocity = new Vector2(0, 0);
        animator.SetBool("IsMoving", false);
    }

    void OnTriggerEnter2D(Collider2D collider){
        //Debug.Log(collider.tag);
        if(collider.tag == "StopTrigger"){
            StopMoving();
            return;
        }
        if(collider.tag == "ArrivedToShop"){
            StopMoving();
            events.kirbyArrivesToShop.Invoke();
            return;
        }
        if(collider.tag == "KirbyObliterator"){
            Destroy(gameObject);
        }
    }

    void OnTriggerExit2D(Collider2D other){
        //Debug.Log(collider.tag);
        if(other.tag == "StopTrigger"){
            StartCoroutine(MoveAgain());
            return;
        }
    }

    private IEnumerator MoveAgain(){
        yield return new WaitForSeconds(Random.Range(0.1f, 1f));
        Move();
        yield return null;
    }
}
