using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class KirbyElevator : MonoBehaviour
{
    private AudioSource audioSource = null;
    public AudioClip kirbyPoyoSound;
    public AudioClip kirbyHiSound;
    public int floor;
    public int goToFloor;
    public float speed = 1f;
    private Rigidbody2D rb;
    private Animator animator;
    public bool isInEelevator;
    public bool arrivedToDestination = false;
    public bool waitingForElevator = false;
    public TMP_Text text;
    void Awake(){

        if(rb == null){
            rb = GetComponent<Rigidbody2D>();
        }
        if(animator == null){
            animator = GetComponent<Animator>();
        }

        audioSource = GetComponent<AudioSource>();
    }

    void Start()
    {
        ElevatorScript.Instance.arrivedAtFloor += LeaveElevator;
        ElevatorScript.Instance.moved += MoveWithElevator;
        Move();
    }

    public void Move(){
        rb.velocity = new Vector2(speed, 0);
        animator.SetBool("IsMoving", true);
    }

    public void StopMoving(){
        rb.velocity = new Vector2(0, 0);
        animator.SetBool("IsMoving", false);
    }

    public void LeaveElevator(int floor){
        if (waitingForElevator)
        {
            if(ElevatorScript.Instance.floor == floor){
                waitingForElevator = false;
                Move();
            }
            return;
        }
        isInEelevator = false;
        arrivedToDestination = true;
        Move();
        EL_KirbyGenerator.Instance.addKirbyButton.interactable = true;
    }
    public void MoveWithElevator(Vector3 pos){
        if(!isInEelevator){
            return;
        }
        transform.position = pos;
    }
    void OnTriggerEnter2D(Collider2D collider){
        //Debug.Log(collider.tag);
        switch(collider.tag){
            case "Elevator":
                if(arrivedToDestination){
                    break;
                }
                StopMoving();
                isInEelevator = true;
                ElevatorScript.Instance.MoveToFloor(goToFloor);
                audioSource.clip = kirbyPoyoSound;
                audioSource.Play();
                //EL_KirbyGenerator.Instance.addKirbyButton.interactable = true;
                break;
            case "WaitForElevator":
                if(floor == ElevatorScript.Instance.floor){
                    break;
                }
                waitingForElevator = true;
                ElevatorScript.Instance.AskElevator(floor);
                audioSource.clip = kirbyHiSound;
                audioSource.Play();
                StopMoving();
                break;
            case "KirbyObliterator":
                Destroy(gameObject);
                break;
        }
    }

    void OnDestroy(){
        ElevatorScript.Instance.arrivedAtFloor -= LeaveElevator;
        ElevatorScript.Instance.moved -= MoveWithElevator;
    }
/*
    void OnTriggerExit2D(Collider2D other){
        //Debug.Log(collider.tag);
        if(other.tag == "StopTrigger"){
            StartCoroutine(MoveAgain());
            return;
        }
    }*/
}
