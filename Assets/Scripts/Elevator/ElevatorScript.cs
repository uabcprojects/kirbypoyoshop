using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using TMPro;

public class ElevatorScript : MonoBehaviour
{
    public Rigidbody2D rbody;
    public TMP_InputField speedMultiplierField;
    public Transform[] elevatorPoints = null;
    public Transform kirbyPinPivot;
    public int floor = 1;
    public int floorToGo = 1;
    public int askForElevatorAt = 0;
    public float defaultSpeed;
    public float speed;
    public bool moving = false;
    public Action<int> arrivedAtFloor;
    public Action<Vector3> moved;
    public static ElevatorScript Instance;

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        floorToGo = floor;
        speed = defaultSpeed;
    }

    public void MoveToFloor(int floorToGo)
    {
        this.floorToGo = floorToGo;
        moving = true;
        float speedMultiplier = float.Parse(speedMultiplierField.text);
        speed = defaultSpeed * speedMultiplier;
    }

    public void AskElevator(int at){
        if(moving){
            askForElevatorAt = at;
        }
        else {
            MoveToFloor(at);
        }
    }

    void FixedUpdate()
    {
        if (!moving) { return; }

        rbody.MovePosition(new Vector2(transform.position.x, transform.position.y + Mathf.Sign(floorToGo - floor) * speed * Time.fixedDeltaTime));
        if(Mathf.Abs(transform.position.y - elevatorPoints[floorToGo - 1].position.y) <= speed * Time.fixedDeltaTime)
        {
            floor = floorToGo;
            transform.position = elevatorPoints[floorToGo - 1].position;
            moving = false;
            arrivedAtFloor?.Invoke(floorToGo);

            if(askForElevatorAt != 0){
                MoveToFloor(askForElevatorAt);
                askForElevatorAt = 0;
            }
        }

        moved?.Invoke(kirbyPinPivot.position);
    }
}
