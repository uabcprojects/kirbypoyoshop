using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class EL_KirbyGenerator : MonoBehaviour
{
    public GameObject kirbyPrefeab;
    public Button addKirbyButton;
    public TMP_Dropdown spawnFloorDropdown;
    public TMP_Dropdown goToFloorDropdown;
    public TMP_InputField kirbySpeedMultiplierField;

    public Transform[] spawnPoints = null;
    public static EL_KirbyGenerator Instance;

    void Awake(){
        Instance = this;
    }

    void Start()
    {
        addKirbyButton.onClick.AddListener(delegate { AddKirby(); });
    }

    public void AddKirby()
    {
        addKirbyButton.interactable = false;
        int spawnFloorValue = spawnFloorDropdown.value;
        int goToFloorValue = goToFloorDropdown.value;
        Debug.Log("Spawn Floor: " + spawnFloorDropdown.options[spawnFloorValue].text);
        Debug.Log("Go To Floor: " + goToFloorDropdown.options[goToFloorValue].text);

        int spawnFloor;
        if(spawnFloorDropdown.options[spawnFloorValue].text == "Random"){
            spawnFloor = Random.Range(1, 4 + 1);
        }
        else {
            spawnFloor = int.Parse(spawnFloorDropdown.options[spawnFloorValue].text);
        }

        int goToFloor;
        if(goToFloorDropdown.options[goToFloorValue].text == "Random"){
            do
            {
                goToFloor = Random.Range(1, 4 + 1);
            } while (goToFloor == spawnFloor);
        }
        else {
            goToFloor = int.Parse(goToFloorDropdown.options[goToFloorValue].text);
        }

        SpawnKirbyAt(spawnFloor, goToFloor);
    }

    public void SpawnKirbyAt(int spawnFloor, int goToFloor)
    {
        GameObject kirby = Instantiate(kirbyPrefeab, spawnPoints[spawnFloor - 1]);
        kirby.GetComponent<KirbyElevator>().floor = spawnFloor;
        kirby.GetComponent<KirbyElevator>().goToFloor = goToFloor;
        kirby.GetComponent<KirbyElevator>().speed *= float.Parse(kirbySpeedMultiplierField.text);
        kirby.GetComponent<KirbyElevator>().text.text = goToFloor.ToString();
        //ElevatorScript.Instance.MoveToFloor(floor);
    }
}
