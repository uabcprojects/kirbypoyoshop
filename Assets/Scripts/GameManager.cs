using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    public static GameManager Instance;

    public void Awake(){
        if(Instance == null){
            Instance = this;
            //DontDestroyOnLoad(this);
        }
        else {
            //this.gameObject = Instance;
            Destroy(gameObject);
        }
    }

    public void GoToScene(string sceneName){
        SceneManager.LoadScene(sceneName);
    }
}
