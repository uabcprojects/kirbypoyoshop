# Playing the game/simulation
Go to [LatestBuild](LatestBuild) folder and download the content. Then, unzip and run the KirbyPoyoShop.exe.

A Selection Menu will appear, where you can select the simulation you want to run.

# Simulations
This project is made in **Unity**.

## Kirby Elevator
A simulation of Kirbys using an elevator. It's interactive: you decide when to spawn Kirbys, on which floor and what floor to go.

YouTube showcase: https://youtu.be/rIeyqtukgS0

![](Images/KirbyElevatorScreen.jpg)

## KirbyPoyoShop
A simulation of Kirbys shopping poyos at the Poyo Shop.

YouTube showcase: https://youtu.be/K-F8vbso7ss

![](Images/poyoShopScreen.jpg)

# Future updates
This repo is for school and will only be updated if I decide to make another homework project in Unity.
